"use strict"
const tabsTitel = document.querySelectorAll(".tabs-title");
const tabsItems = document.querySelectorAll(".tabs-item");
const onTabClick = (event)=>{
    const pastTab = document.querySelector(".active");
    pastTab.classList.remove("active");
    event.target.classList.add("active");
    const list = document.querySelector(".tabs-content")
    const pastItem = list.querySelector(".active");
    pastItem.classList.remove("active");
    tabsItems.forEach((element)=>{
        if(element.dataset.name === event.target.textContent){
            element.classList.add("active")
        }
    });
};

for (const tab of tabsTitel) {
    for(const item of tabsItems){
        if(item.dataset.name === undefined){
            item.setAttribute("data-name", tab.textContent);
            break;
        }
    } 
    tab.addEventListener('click', onTabClick);
    tab.onmousedown=function () {
        return false;
    }
    // tab.addEventListener('mousedown', ()=>{return false});
}

// маю сумніви щодо мого рішення присваювати значення data-name єлементам списку по черзі 
// бо в проекті вони можуть йти не по черзі, але мені здалось це логічним.
// також я намагався додати ще один обробник подій кожному tab, щоб заборонити віділення,
// але через addEventListener не виходить
